-- http://pastebin.com/Ay94hzhY
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX

-- READ PARAMETERS
local args = { ... }
local argsOK = true
 
if #args < 1 then
 argsOK = false
else
 myhoogte = math.floor(tonumber(args[1]) or 1)
 mybreedte = math.floor(tonumber(args[2]) or 1)
end

-- STOP EXECUTION
if argsOK == false then
 print( "Usage: muur hoogte" )
 print( "put in inventory: stone - stone - stone - ....")
 print( "                  stair - slab  - stair - ....")
 print( "                  stone - fence - stone - ....")
 print( "                  stair - slab  - stair - ....")
 return
end

-- face outside
function build()
 -- clear below 
 t.digDown()
 t.digAndUp()
 
 t.select(5)
 t.placeUp() -- stair
 
 t.select(13)
 t.placeDown() -- stair
 
 t.back()
 
 t.select(9)
 t.place() -- stone
 
 t.turn()
 t.digAndForward()
 
 t.digUp()
 t.select(7)
 t.placeUp() -- stair
 
 t.digDown()
 t.select(15)
 t.placeDown() -- stair
 
 t.back()
 
 t.select(11)
 t.place() -- stone
 
 t.select(14)
 t.placeDown() -- slab
 
 t.digAndUp()
 
 t.select(10)
 t.placeDown() -- iron fence
 
 t.digAndUp()
 
 t.select(6)
 t.placeDown() -- slab
 
-- t.select(3)
-- t.place()
 
 t.turn()
 
-- t.select(1)
-- t.place()
 
 t.digAndUp()
 
-- t.select(2)
-- t.placeDown()
 
 t.digAndForward()
 
end

b = 0
while b < mybreedte do

h = 0
while h < myhoogte do
 build()
 h = h + 1
end

t.turn()
t.down()
t.forward() -- mid

t.select(1)
t.place()

t.back() -- right
t.select(2)
t.place()

t.back() -- now at wood column
t.select(3)
t.place()

-- go down and place wood
h = 0 
while h < myhoogte do

t.digAndDown() -- stair
t.select(4)
t.placeUp()

t.digAndDown() -- iron fence
t.select(8)
t.placeUp()

t.digAndDown() -- stair
t.select(16)
t.placeUp()

t.digAndDown() -- stone
t.select(12)
t.placeUp()

t.forward()
t.forward()

t.select(1)
t.place()

t.back()
t.select(2)
t.place()

t.back()
t.select(3)
t.place()

 h = h + 1
end

t.turn()
t.digAndForward()
t.digAndForward()
t.digAndForward()
t.digAndUp()

b = b + 1
end