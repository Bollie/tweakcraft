-- http://pastebin.com/TbfjL9bA
-- http://pastebin.com/u/Bollie
os.loadAPI("p")

-- Put 10 seeds in slot 1. Harvie will keep seeds in slot 1
local args = { ... }
if #args < 2 or #args > 4 then
 print("usage: harvie <length> <width> <1=chest, default=0> <999=forever, default=1>")
 return
end
local mylength = tonumber(args[1])
local mywidth = tonumber(args[2])

local usechest = true
if (tonumber(args[3]) or 0) == 0 then
 usechest = false
end

local forever = tonumber(args[4]) or 1
-- END PARAMETERS

-- FUNCTION HARVEST
function harvest(width, length)
 for w = 1, width do
  for l = 1, length do
   -- Harvest and place seed (slot 1)
   turtle.digDown()
   turtle.select(1)
   turtle.placeDown()
   -- goto next location
   if l < length then
    turtle.forward()
   else
    -- end of length, turn (except for last w)
    if w < width then
     if w % 2 == 0 then
      turtle.turnLeft()
      turtle.forward()
      turtle.turnLeft()
     else
      turtle.turnRight()
      turtle.forward()
      turtle.turnRight()
     end -- turn
    end -- last w
   end --last l
  end -- all l(ength)
 end -- all w(idth)
end

-- FUNCTION RETURNHOME
function returnhome(width, length)
 -- if uneven, return to begin l)
 if width % 2 == 1 then
  turtle.turnLeft()
  turtle.turnLeft()
  for l = 1, length - 1 do
   turtle.forward()
  end
 end
 -- return to home
 turtle.turnRight()
 for w = 1, width - 1 do
  turtle.forward()
 end
 turtle.turnRight()
end

-- FUNCTION EMPTYSLOTS
function emptyslots()
 turtle.turnLeft()
 turtle.turnLeft()
 -- keep 10 seeds in slot 1
 if turtle.getItemCount(1) > 10 then
  turtle.select(1)
  turtle.drop(turtle.getItemCount(1) - 10)
 end
 -- empty other
 for slot = 2, 16 do
  turtle.select(slot)
  turtle.drop()
 end
 -- make ready for next time
 turtle.turnRight()
 turtle.turnRight()
end 

-- MAIN

while forever > 0 do

msg = "Harvie: Start "..mylength.."x"..mywidth.."="..mylength*mywidth
if usechest then
 msg = msg.." in chest"
end
p.send(msg)

harvest(mywidth, mylength)
returnhome(mywidth, mylength)
if usechest then
 emptyslots()
end 

msg = "Harvie: Finished "..mylength.."x"..mywidth.."="..mylength*mywidth
if usechest then
 msg = msg.." in chest"
end
p.send(msg)

forever = forever - 1
if forever > 0 then
 sleep(4500)
end
end