-- http://pastebin.com/2mup40mY
-- http://pastebin.com/u/Bollie
function GetScript(pb, name)
 shell.run("delete "..name)
 sleep(0.5) -- 10 ticks
 shell.run("pastebin get "..pb.." "..name)
 sleep(0.5) -- 10 ticks
end

local scripts = {}
 scripts['p']         = "SGcYmCqd" -- Peripheral API bij Bollie
 scripts['t']         = "6vxbgdjX" -- Turtle API by Bollie
 scripts['start']     = "xS4BJUb9" -- startup script
-- scripts['OreQuarry'] = "RwiktQhs" -- digs every third layer and checks down/up for ore (version by Bollie)
-- scripts['uwc']       = "Kngv6tWj" -- Ultimate Wood Chopper (version by Bollie)
 scripts['floor']     = "E3DaJR3S" -- Bollie, builds floor
-- scripts['harvie']    = "TbfjL9bA" -- Bollie, wheat farmer
 scripts['sand']      = "3cqEdufx" -- Bollie, wheat farmer
 scripts['listen']    = "WsE0EZfX" -- Bollie, listen to rednet
-- scripts['fuel']      = "JLCh0V8k" -- Bollie, fuel at charging station
 scripts['bed']       = "HdkYq3Pw" -- Bollie, dig to bedrock and quarry up
-- scripts['quarry']    = "YkH0UmMD" -- Bollie, setup quarry platforms and machines
 scripts['wall']      = "dXGebSgh" -- Bollie, build wall
-- scripts['tank']      = "5wxkkpVV" -- Iron tank
-- scripts['muur']      = "Ay94hzhY" -- muur 
 scripts['stairs']    = "xeEEDU4T" -- Stairs down incl placing stairs
 scripts['docs']      = "aeajqY5G" -- Docs openperipherals to screen
 scripts['rock']      = "7ag091Gh" -- bedrock breaker
 scripts['glass']     = "dsCvTxyg" -- test glasses
 scripts['aeguard']   = "4Lyhu97q" -- guard minimum amount of items in AE
 scripts['json']      = "qn3cUHX5" -- json services: http://regex.info/code/JSON.lua
 for key,value in pairs( scripts ) do
  print(key.." ("..value..")")
 end
 
 -- READ PARAMETERS
local args = { ... }
if #args == 0 then
 for key,value in pairs( scripts ) do
  GetScript(value, key)
 end
 
else
 -- load scripts with name given as parameter
 for scriptname, scriptid in pairs(scripts) do
  if ((scriptname == args[1]) or (scriptid == args[1])) then
     GetScript(scriptid, scriptname)
  end
 end

end