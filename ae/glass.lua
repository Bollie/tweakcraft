-- http://pastebin.com/dsCvTxyg
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd

-- with glasses and ae, shows for all items in ae an icon and the quantity, each item 1 sec

local glass = p.findPeripheral("openperipheral_glassesbridge")
if glass == nil then do return end end -- p will print msg

local ae = p.findPeripheral("appeng_me_tileterminal")
if ae == nil then do return end end -- p will print msg
 
glass.clear()
local gIcon = glass.addIcon(1,1,1,0) -- x,y,id,meta
local gTxt1 = glass.addText(1, 20, "0", 0xFFFF00)

while true do
 for k,item in pairs(ae.getAvailableItems()) do
   gIcon.setItemId(item["id"])
   gIcon.setMeta(item["dmg"])
   gTxt1.setText(string.format("%d", item["qty"]))
   os.sleep(1)
 end
end