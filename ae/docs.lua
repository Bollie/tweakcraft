-- http://pastebin.com/TbfjL9bA
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd

--[[
  Made by SinZ and boq
--]]
local args = {...}
if #args == 0 then
  print("usage: docs <side> (function)")
  return
end

local side = args[1]
local pe = peripheral.wrap(side)

if not pe then
  print("No peripheral on '" .. side .. "'")
  return
end

if not pe.getAdvancedMethodsData then
 print("Peripheral '" .. peripheral.getType(side) .. "' is not OpenPeripheral(TM)")
 return
end

local info = pe.getAdvancedMethodsData()

function argName(arg)
  if arg.vararg then
    return arg.name.."..."
  elseif arg.optional then
    return arg.name.."?"
  else
    return arg.name
  end
end

if #args == 1 then
  for name,data in pairs(info) do
    args = {}
    for _,arg in pairs(data.args) do
      table.insert(args, argName(arg))
    end
    p.send(name.."("..table.concat(args,",")..")")
  end
else --must be 2 or more
  for name,data in pairs(info) do
    if args[2]:lower() == name:lower() then
      p.send(name..": "..data.description)
      p.send("returns: "..string.lower(table.concat(data.returnTypes,',')))
      if #data.args > 0 then
        p.send("args: ")
        for _,arg in ipairs(data.args) do
          p.send(" - ("..arg.type:lower()..")"..argName(arg)..": "..arg.description)   
        end
      end
    end
  end
end