-- http://pastebin.com/4Lyhu97q
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd

-- checks for minimum amount of item in AE

-- define items to watch here:
items = {}
items[49] = {itemId=49,dmgValue=0,name="Obsidian",targetQty=64}
items[263] = {itemId=263,dmgValue=0,name="Coal",targetQty=64}
items[264] = {itemId=264,dmgValue=0,name="Diamond",targetQty=64}
items[331] = {itemId=331,dmgValue=0,name="Redstone",targetQty=64}
items[348] = {itemId=348,dmgValue=0,name="Glowstone Dust",targetQty=64}
items[351] = {itemId=351,dmgValue=0,name="Ink Sac",targetQty=64}
items[3511] = {itemId=351,dmgValue=1,name="Rose Red",targetQty=64}
items[3512] = {itemId=351,dmgValue=2,name="Cactus Green",targetQty=64}
items[3514] = {itemId=351,dmgValue=4,name="Lapis Lazuli",targetQty=64}
items[3515] = {itemId=351,dmgValue=5,name="Purple Dye",targetQty=64}
items[3516] = {itemId=351,dmgValue=6,name="Cyan Dye",targetQty=64}
items[3517] = {itemId=351,dmgValue=7,name="Light Gray Dye",targetQty=64}
items[3518] = {itemId=351,dmgValue=8,name="Gray Dye",targetQty=64}
items[3519] = {itemId=351,dmgValue=9,name="Pink Dye",targetQty=64}
items[35110] = {itemId=351,dmgValue=10,name="Lime Dye",targetQty=64}
items[35111] = {itemId=351,dmgValue=11,name="Dandelion Yellow",targetQty=64}
items[35112] = {itemId=351,dmgValue=12,name="Light Blue Dye",targetQty=64}
items[35113] = {itemId=351,dmgValue=13,name="Magenta Dye",targetQty=64}
items[35114] = {itemId=351,dmgValue=14,name="Orange Dye",targetQty=64}
items[368] = {itemId=368,dmgValue=0,name="Ender Pearl",targetQty=64}
items[388] = {itemId=388,dmgValue=0,name="Emerald",targetQty=64}
items[902] = {itemId=902,dmgValue=0,name="Certus Quartz Ore",targetQty=64}
items[13981] = {itemId=1398,dmgValue=1,name="Copper Ore",targetQty=64}
items[13982] = {itemId=1398,dmgValue=2,name="Tin Ore",targetQty=64}

-- Guard the level of items en reorder if necessary
local ae = p.findPeripheral("appeng_me_tilecontroller")
if ae == nil then do return end end -- p will print msg

-- Check whether AE is already making receipts
function isFree()
 local jobs = ae.getJobList()
 if(next(jobs)) == nil then
  return true
 end
 return false
end

-- Main check for level of item en requesting more
function CheckLevel(item)
 while not isFree() do
  sleep(1)
 end
 currentQty = ae.countOfItemType(item.itemId,item.dmgValue)
 if currentQty < item.targetQty then
  p.send("Adding "..item.name..": "..currentQty.."->"..item.targetQty)
  ae.requestCrafting({id=item.itemId,dmg=item.dmgValue,qty=item.targetQty-currentQty})
 end
end

-- main loop (no end)
while true do
 for _,item in pairs(items) do
  CheckLevel(item)
  sleep(1)
 end
end