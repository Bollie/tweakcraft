-- http://pastebin.com/E3DaJR3S
-- http://pastebin.com/u/Bollie
os.loadAPI("t")
os.loadAPI("p")
--
local tArgs = { ... }
if #tArgs < 2 then
    print( "Usage: floor <length> <width> <1=up, other down>" )
    print( "Slot 1: blocks. Other slots: Enderchest, torches, more blocks")
    return
end

local maxi = tonumber( tArgs[2] )
local maxj = tonumber( tArgs[1] )

placeD = true -- default place Down
if tonumber(tArgs[3]) == 1 then
    placeD = false
end

-- this block will be used
local blockToUseSlot = 1
if t.getItemCount(blockToUseSlot) == 0 then
    print( "ERROR: No blocks in slot 1")
    exit()
end
local blockToUseName = {}
blockToUseName[1] = t.getItemDetail(blockToUseSlot)
print(blockToUseName[1].name)

-- Refill slot 1 with more items in slot 2-16
function findAndTransferItems()
    t.select(1)
    i = 16
    while i > 1 do
        -- found slot with same item as in slot 1
        if t.compareTo(i) == true then
            t.select(i)
            itemcount = t.getItemCount(i)
            t.transferTo(1, itemcount)
            t.select(1)
            return true
        end
        i = i - 1
    end
    t.select(1)
    return false
end

-- FUNCTION EMPTYSLOTS
function emptyslots(checkslot)

    -- check for enderchest
    local enderchestslot = t.findEnderchest()
    if enderchestslot == 0 then
        return false
    end

    t.digsUp()
    t.select(enderchestslot)
    t.placeUp()

    for i = 1, 16 do
        if (t.getItemCount(i) > 0) then
            if t.getItemDetail(i).name ~= blockToUseName[1].name then
                t.dropAll(t.dropUp, 0, i, i)
            end
        end
    end
    t.select(enderchestslot)
    t.digUp()
    t.select(1)
end

function placeBlock()

    -- check always for right block
    blockToUseSlot = t.findItemDetail(blockToUseName)
    if blockToUseSlot == 0 then
        print("ARG empty!")
        exit()
    end
    t.select(blockToUseSlot)

    -- place block
    if placeD then
        if (t.detectDown()) and (t.compareDown() == false) then
            t.digDown()
        end
        t.placeDown()
    else
        if (t.detectUp()) and (t.compareUp() == false) then
            t.digUp()
        end
        t.placeUp()
    end

end

function tryPlaceTorch()
    local torchslot = t.findTorch()
    if torchslot == 0 then
        return
    end
    -- yeah place torch
    t.turn()
    t.select(torchslot)
    t.place()
    t.turn()
    -- get slot
    blockToUseSlot = t.findItemDetail(blockToUseName)
    t.select(blockToUseSlot)
end

-- Start here
p.send("Floor: Start "..maxi.." x "..maxj.." = "..maxi*maxj)
for i = 1, maxi do
    for j = 1, maxj do
        placeBlock()
        -- nog bezig met lijn
        if j < maxj then
            t.digAndForward()
            if placeD and ((j % 5) == 3) and ((i % 5) == 3) then
                tryPlaceTorch()
            end
        else
            -- eind lijn
            if (i < maxi) then
                -- make turn
                if (i % 2) == 1 then
                    t.turnRight()
                    t.digAndForward()
                    t.turnRight()
                else
                    t.turnLeft()
                    t.digAndForward()
                    t.turnLeft()
                end
            else
                -- helemaal eind
                -- oneven: eerst terug
                if (i % 2) == 1 then
                    t.turnRight()
                    t.turnRight()
                    for k = 2, maxj do
                        t.digAndForward()
                    end
                end
                -- even of oneven keer
                t.turnRight()
                for k = 2, maxi do
                    t.digAndForward()
                end
                t.turnRight()
                break
            end
        end
    end
end

-- emptyslots to enderchest
-- emptyslots()

p.send("Floor: Finished "..maxi.." x "..maxj.." = "..maxi*maxj)
