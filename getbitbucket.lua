-- BitBucket downloading utility for CC.
local args = {...}
local automated = true                          -- Don't touch!
local hide_progress = false                     -- If true, will not list out files as they are downloaded
local pre_dl = "print('Starting download...')"  -- Command(s) to run before download starts.
local post_dl = "print('Download complete!')"   -- Command(s) to run after download completes.

-- configuration
args[1] = "Bollie"                              -- Bitbucket username
args[2] = "tweakcraft"                          -- Bitbucket repo name
args[3] = nil                                   -- Branch - defaults to "master"
args[4] = nil                                   -- Path - defaults to root ("/")

-- default configuration
args[3] = args[3] or "master"
args[4] = args[4] or ""

if not automated and #args < 2 then
    print("Usage:\n"..shell.getRunningProgram().." <user> <repo> [branch=master] [path=/]")
    error()
end

-- functions
local function save(data,file)
    local file = shell.resolve(file)
    local path = string.sub(file,1,#file - #fs.getName(file))
    if not (fs.exists(path) and fs.isDir(path)) then
        if fs.exists(path) then
            fs.delete(path)
        end
        fs.makeDir(path)
    end
    local f = fs.open(file,"w")
    f.write(data)
    f.close()
end

local function download(url, file)
    save(http.get(url).readAll(),file)
end

-- load JSON
if not fs.exists("json") then
    print("Downloading JSON api")
    download("http://pastebin.com/raw.php?i=qn3cUHX5","json")
end
os.loadAPI("json")

if pre_dl then loadstring(pre_dl)() else print("Downloading files from bitbucket....") end
download("https://bitbucket.org/api/1.0/repositories/"..args[1].."/"..args[2].."/src/master/", "bitbucket/available")
local data = json.decodeFromFile("bitbucket/available")
if data == nil then
    error("Invalid repository",2)
else
    local path = data.path
    for k,v in pairs(data) do
        -- Make directories
        if k == "directories" then
            for _,newpath in pairs(v) do
                fs.makeDir(fs.combine(path,newpath))
                if not hide_progress then
                    print(fs.combine(path,newpath))
                end
            end
        end
    end
    for k,v in pairs(data) do
        -- Download files
        if k == "files" then
            for _,file in pairs(v) do
                local basefile = string.gsub(file.path,".lua","")
                download("https://bitbucket.org/api/1.0/repositories/"..args[1].."/"..args[2].."/raw/"..args[3].."/"..file.path,fs.combine(path,basefile))
                if not hide_progress then
                    print(fs.combine(path,basefile))
                end
            end
        end
    end
end
if post_dl then
    loadstring(post_dl)()
end