-- http://pastebin.com/dXGebSgh
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX

local MAXSLOT = 16
local tArgs = {...}
local height = tonumber(tArgs[2]) 
local length = tonumber(tArgs[1])
local slot = 1

-- Start program
p.send("Build wall. Length: "..length.." Height: "..height)
t.select(slot)
t.digAndUp()

for j = 1, height, 1 do
 for i = 1, length, 1 do
    
  -- check for items to place
  if (t.getItemCount(slot) == 0) then
   slot = 1
   while (slot < MAXSLOT) and (t.getItemCount(slot) == 0) do
    slot = slot + 1
   end
   -- check for success
   if (t.getItemCount(slot) == 0) then
    p.send("All slots are empty")
    error() -- exit script
   else
    t.select(slot)
   end
  end

  -- place
  t.placeDown()
    
  -- move
  if i < length then
   t.digAndForward()
  else
   t.turn()  
   if j < height then
    t.digAndUp()
   end
  end

 end -- length
end -- height