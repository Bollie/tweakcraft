-- http://pastebin.com/5G4D05r2
-- http://pastebin.com/u/Bollie

function dropempty()
 return turtle.drop() -- forward chest for empty buckets
end

function suckfull()
 return turtle.suckDown() -- down chest for full buckets
end

function emptyslots()
 local j 
 for j = 1, 16 do
  turtle.select(j)
  dropempty()
 end
end

emptyslots()

while (turtle.getFuelLevel() < 40000) do
 turtle.select(1)
 if suckfull() then
  turtle.refuel()
  print(turtle.getFuelLevel())
  turtle.select(1)
  dropempty()
 else
  os.sleep(4)
 end
end

turtle.up()
turtle.turnRight()
turtle.forward()