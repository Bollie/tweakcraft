-- http://pastebin.com/6vxbgdjX
-- http://pastebin.com/u/Bollie

-- turtle interface

-- to be added:
-- 1. vector+f (x,y,z,f) -> initialise + trace (forward/back/up/down/turnRight/turnLeft) + info
-- 2. fuel-> check slots for fuel: refuel(0)=true
-- 3. slots -> drop all, count all items
-- 4. move+ -> dig+move

-- position and direction in world
x = 0 -- +east / -west
y = 0 -- vertical
z = 0 -- +south / -north
f = 0
-- 0 = South
-- 1 = West
-- 2 = North
-- 3 = East
local FX = { 0, -1, 0, 1 }
local FZ = { 1, 0, -1, 0 }

function position()
    return vector.new(x, y, z)
end

function posx()
    return x
end

function posy()
    return y
end

function posz()
    return z
end

function posf()
    return f
end

function gotoPos(x, y, z, f)

    -- if position to go is 3+ higher go up 3x first
    if y > (posy() + 2) then
        digAndUp()
        digAndUp()
        digAndUp()
    end

    -- go down if necessary
    while y < posy() do
        digAndDown()
    end

    -- go z+
    if posz() < z then
        while posf() ~= 0 do
            turnLeft()
        end
        while posz() < z do
            digAndForward()
        end
    end

    -- go x+
    if posx() < x then
        while posf() ~= 3 do
            turnLeft()
        end
        while posx() < x do
            digAndForward()
        end
    end

    -- go z-
    if z < posz() then
        while posf() ~= 2 do
            turnLeft()
        end
        while z < posz() do
            digAndForward()
        end
    end

    -- go x-
    if x < posx() then
        while posf() ~= 1 do
            turnLeft()
        end
        while x < posx() do
            digAndForward()
        end
    end

    -- face
    while posf() ~= f do
        turnLeft()
    end

    -- go up
    while y > posy() do
        digAndUp()
    end
end

-- *********************
-- original functions
-- *********************

-- CRAFT
function craft(quantity)
    return turtle.craft(quantity)
end

-- MOVEMENT
function forward()
    local success = turtle.forward()
    if success then
        x = x + FX[f+1]
        z = z + FZ[f+1]
    end
    return success
end

function back()
    local success = turtle.back()
    if success then
        x = x - FX[f+1]
        z = z - FZ[f+1]
    end
    return success
end

function up()
    local success = turtle.up()
    if success then
        y = y + 1
    end
    return success
end

function down()
    local success = turtle.down()
    if success then
        y = y - 1
    end
    return success
end

-- TURN
function turnLeft()
    f = (f + 3) % 4
    return turtle.turnLeft()
end

function turnRight()
    f = (f + 1) % 4
    return turtle.turnRight()
end

-- SLOTS
function select(slotNum)
    return turtle.select(slotNum)
end

function getSelectedSlot()
    return turtle.getSelectedSlot()
end

function getItemCount(slotNum)
    return turtle.getItemCount(slotNum)
end

function getItemSpace(slotNum)
    return turtle.getItemSpace(slotNum)
end

function getItemDetail(slotNum)
    return turtle.getItemDetail(slotNum)
end

-- EQUIP
function equipLeft()
    return turtle.equipLeft()
end

function equipRight()
    return turtle.equipRight()
end

-- ATTACK
function attack()
    return turtle.attack()
end

function attackUp()
    return turtle.attackUp()
end

function attackDown()
    return turtle.attackDown()
end

-- DIG
function dig()
    return turtle.dig()
end

function digUp()
    return turtle.digUp()
end

function digDown()
    return turtle.digDown()
end

-- PLACE
-- implementation of signText missing!
function place(signText)
    return turtle.place(signText)
end

function placeUp()
    return turtle.placeUp()
end

function placeDown()
    return turtle.placeDown()
end

-- DETECT AND COMPARE
function detect()
    return turtle.detect()
end

function detectUp()
    return turtle.detectUp()
end

function detectDown()
    return turtle.detectDown()
end

-- INSPECT
function inspect()
    return turtle.inspect()
end

function inspectUp()
    return turtle.inspectUp()
end

function inspectDown()
    return turtle.inspectDown()
end

-- COMPARE
function compare()
    return turtle.compare()
end

function compareUp()
    return turtle.compareUp()
end

function compareDown()
    return turtle.compareDown()
end

function compareTo(slot)
    return turtle.compareTo(slot)
end

-- DROP AND SUCK
function drop(count)
    return turtle.drop(count)
end

function dropUp(count)
    return turtle.dropUp(count)
end

function dropDown(count)
    return turtle.dropDown(count)
end

function suck()
    return turtle.suck()
end

function suckUp()
    return turtle.suckUp()
end

function suckDown()
    return turtle.suckDown()
end

-- FUEL
function refuel(quantity)
    return turtle.refuel(quantity)
end

function getFuelLevel()
    return turtle.getFuelLevel()
end

function getFuelLimit()
    return turtle.getFuelLimit()
end

function transferTo(slot, quantity)
    return turtle.transferTo(slot, quantity)
end

--[[



]]--

-- turn 180 degree
function turn()
    return turnRight() and turnRight()
end

-- keep digging till nothing detected
function digMulti(fnDetect, fnDig)
    local retvalue = 0
    local success = false
    while fnDetect() do
        if fnDig() then
            retvalue = retvalue + 1
        end
        sleep(0.6)
    end
    return retvalue
end

function digs()
    return digMulti(detect, dig)
end

function digsUp()
    return digMulti(detectUp, digUp)
end

function digsDown()
    return digMulti(detectDown, digDown)
end

-- dig and move
function digAndGo(fnDetect, fnDig, fnGo, fnAttack)
    if fnDetect() == true then fnDig() end
    while fnGo() ~= true do
        fnDig()
        fnAttack()
        sleep(0.2)
    end
    return true
end

function digAndForward()
    return digAndGo(detect, dig, forward, attack)
end

function digAndUp()
    return digAndGo(detectUp, digUp, up, attackUp)
end

function digAndDown()
    return digAndGo(detectDown, digDown, down, attackDown)
end

-- Extra function to drop many slots. Example
-- countSlots, countItems = dropAll(t.dropUp, 0, 1, 16)
function dropAll(fnDrop, keepItems, firstSlot, lastSlot)
    fnDrop = fnDrop or drop
    keepItems = keepItems or 0
    firstSlot = firstSlot or 1
    lastSlot = lastSlot or 16
    --
    local countSlots = 0 -- return how many slots where dropped
    local countItems = 0 -- return how many items where dropped
    local itemCount = 0
    --
    for slot = firstSlot, lastSlot do
        itemCount = getItemCount(slot)
        -- drop if more
        if itemCount > keepItems then
            t.select(slot)
            fnDrop(itemCount - keepItems)
            countSlots = countSlots + 1
            countItems = countItems + (itemCount - keepItems)
        end
    end
    --
    return countSlots, countItems
end

-- Finds first slot with (one of) specified item(s)
-- input: Table { { name, damage }, .. }
-- output: slotnumnber, 0 if not found
-- Example: slotNum = findItemDetail({ "EnderStorage:enderChest" })
function findItemDetail(itemsToFind)
    -- check all slots
    for i = 16, 1, -1 do
        -- slot contains item
        if turtle.getItemCount(i) > 0 then
            slotitem = turtle.getItemDetail(i)
            -- check the slot contains right item
            for _,item in pairs(itemsToFind) do
                if (slotitem.name == item.name and slotitem.damage == item.damage) then
                    return i
                end
            end
        end
    end
    return 0
end

-- extra function to find enderchest
function findEnderchest()
    return findItemDetail({ { ["name"] = "EnderStorage:enderChest", ["damage"] = 0 } })
end

-- extra function to find torch
function findTorch()
    return findItemDetail({ 
       { ["name"] = "TConstruct:decoration.stonetorch", ["damage"] = 0 }
      ,{ ["name"] = "minecraft:torch", ["damage"] = 0 } 
       })
end
