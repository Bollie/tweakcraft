-- http://pastebin.com/SGcYmCqd
-- http://pastebin.com/u/Bollie

-- How to use? 
-- 1. Put os.loadAPI("p") in top of your code
-- 2. p.send("Hello world!") etc with p. as call

MYCHANNEL = 30609 -- change this to your magical number
modem = nil -- will be set on first use
mon = nil -- will be set on first use

-- ********************
--  modem
-- ********************
function initModem()
 if modem == nil then
  modem = findPeripheral("modem")
 end
end

-- SENDING
function send(msg)
 print(msg)
 initModem()
 if modem ~= nil then
  modem.transmit(MYCHANNEL, os.getComputerID(), msg)
 end
end

function listen(channel)
 initModem()
 channel = channel or MYCHANNEL
 --
 if not modem.isOpen(channel) then
  modem.open(channel)
 end
 --
 local event, modeSide, senderChannel, replyChannel, message, senderDistance = os.pullEvent("modem_message")
 -- output
 monwrite(mcDT().." "..replyChannel..": "..message)
end 
 
-- ********************
--  monitor
-- ********************
function initMonitor()
 if mon == nil then
  mon = findPeripheral("monitor")
  if mon ~= nil then
   mon.setTextScale(1.0)
   mon.clear()
   mon.setCursorPos(1,1)
   
   x, y = mon.getSize()
   mon.write(os.getComputerID().." IS READY: "..x.." x "..y)
   newLine()
  end 
 end
end

function newLine()
 local _,cY= mon.getCursorPos()
 local _,mY = mon.getSize()
 cY = cY + 1
 if cY > mY then
  cY = 1
 end
 mon.setCursorPos(1,cY)
 mon.clearLine()
end

function monwrite(msg)
 initMonitor()
 if mon ~= nil then
  mon.write(msg)
  newLine()
 end
end

-- ********************
--  drive
-- ********************
function findDrive()
 return findPeripheral("drive")
end

-- ********************
--  common
-- ********************
function findPeripheral(pType)
 for i, v in pairs(rs.getSides()) do
  if peripheral.getType(v) == pType then
   return peripheral.wrap(v)
  end
 end
 -- not found
 print("ERROR: not found '"..pType.."'")
 return nil
end

function mcDT()
 -- Outputs: 1023:05:45 The daynumber can be high. Extra 0 when hour is 1 digit
 return os.day()..":"..string.sub("0"..textutils.formatTime(os.time(),true),-5)
end

function online()
 send("Online: "..os.getComputerLabel().." Fuel: "..turtle.getFuelLevel())
end