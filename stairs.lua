-- http://pastebin.com/xeEEDU4T
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX
 
-- READ PARAMETERS
local args = { ... }
 
-- CHECK SLOTS AND PARAMETERS
local argsOK = true
 
if #args < 1 then
 argsOK = false
else
 -- read length
 mylength = math.floor(tonumber(args[1]) or 1)
end

placeChairs = false
if (t.getItemCount(16) > 0) then
 placeChairs = true
end

if argsOK == false then
 print("usage: stairs <length>")
 print("put chairs in slot 16")
 return
end
-- END PARAMETERS

-- xxxxxxxxx
-- xxxxx. <= . will be digged (1 up 3 down)
-- xxxx..t <= start
-- xxx...
-- xx....
-- x.....xxx
-- .....xxxx
-- x...xxxxx

for d = 1, mylength do
 t.digAndForward()
 t.digUp()
 t.digAndDown()
 t.digAndDown()
 t.digDown()
 t.digAndUp()
end

-- return
t.turn()
t.digAndDown()

t.select(16)
for d = 1, mylength do
 if placeChairs == true then
  t.placeDown()
 end
 t.digAndUp()
 t.digAndForward()
end