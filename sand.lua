-- http://pastebin.com/TbfjL9bA
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX

local mylength = 1
local mywidth = 1
local mydepth = 1
local usechest = false

local ENDERCHESTSLOT = 16

-- READ PARAMETERS
local args = { ... }

-- CHECK SLOTS AND PARAMETERS
local argsOK = true

if #args < 2 then 
 argsOK = false
else
 -- read length, width and depth (this is 1/3 of real depth. So put in 2 for digging 6, etc)
 mylength = math.floor(tonumber(args[1]) or 1)
 mywidth = math.floor(tonumber(args[2]) or 1)
 mydepth = math.floor(tonumber(args[3]) or 1)
 -- do sanity check
 if (mylength < 1) or (mywidth < 1) or (mydepth < 1) then argsOK = false end
 -- boundary is arbitrairy, but protects user for bad input
 if (mylength > 64) or (mywidth > 64) or (mydepth > 24) then argsOK = false end 
end

if (tonumber(args[4]) or 0) == 1 then
 usechest = true
end
-- check for ender chest
if usechest and (t.getItemCount(ENDERCHESTSLOT) == 0) then 
 argsOK = false 
end 

if argsOK == false then
 print("put ender chest in slot "..ENDERCHESTSLOT..".")
 print("usage: sand <length> <width> <optional depth=1> <optional 1=chest>")
 return
end
-- END PARAMETERS

-- FUNCTION HARVEST
function harvest(width, length, currentdepth, w, l)
 while w <= width do
  while l <= length do
  
   -- dig down
   t.select(1)
   if t.compareDown() == false then
    t.digDown()
   end
   -- dig up
   t.select(1)
   if t.compareUp() == false then
    t.digsUp()
   end
   
   if usechest == true then
    emptyslots(10)
   else
    emptyslots(15) -- only empty all slots when slot 15 is not empty anymore
   end
   
   -- goto next location
   if l < length then
    t.digAndForward()
   else
    -- end of length, turn (except for last w)
    if w < width then
     if ((currentdepth + 1) * (width + 1) + w) % 2 == 0 then
      t.turnLeft()
      t.digAndForward()
      t.turnLeft()
     else
      t.turnRight()
      t.digAndForward()
      t.turnRight()
     end -- turn
    end -- last w
   end --last l

   l = l + 1
  end -- all l(ength)
  l = 1
  w = w + 1 
 end -- all w(idth)
end

-- FUNCTION RETURNHOME
function returnhome(width, length, currentdepth)
 -- if uneven, return to begin l)
 if (currentdepth % 2 == 1) and (width % 2 == 1) then
  t.turn()
  for l = 1, length - 1 do
   t.digAndForward()
  end
 end
 -- return to home
 if (currentdepth % 2 == 1) then
  t.turnRight()
  for w = 1, width - 1 do
   t.digAndForward()
  end
  t.turnRight()
 end
end

-- FUNCTION EMPTYSLOTS
function emptyslots(checkslot)
 if checkslot ~= nil then
  if t.getItemCount(checkslot) == 0 then
   return
  end   
 end
 if usechest == true then
  t.digsUp()
  t.select(ENDERCHESTSLOT)
  t.placeUp()
  t.dropAll(t.dropUp, 1, 1, 1)
  t.dropAll(t.dropUp, 0, 2, 16)
  t.select(ENDERCHESTSLOT)
  t.digUp()
  t.select(1)
 else
  tmp_x = t.posx()
  tmp_y = t.posy()
  tmp_z = t.posz()
  tmp_f = t.posf()
  
  t.gotoPos(0, 0, 0, 2)
  t.dropAll(t.drop, 1, 1, 1)
  t.dropAll(t.drop, 0, 2, 16)
  t.select(1)
  t.gotoPos(tmp_x, tmp_y, tmp_z, tmp_f)

 end
end 

-- MAIN

msg = "sandie: Start "..mylength.."x"..mywidth.."x"..mydepth.."="..mylength*mywidth*mydepth
if usechest then
 msg = msg.." in enderchest"
end
p.send(msg)

for d = 1, mydepth do
 t.select(1)
 w = 1
 l = 1
 harvest(mywidth, mylength, d, w, l)
 -- next layer
 if d < mydepth then
  t.digAndDown()
  t.digAndDown()
  t.digAndDown()
  t.turn()
 end
end


-- get to shaft
returnhome(mywidth, mylength, mydepth)
-- get up
for d = 1, mydepth - 1 do
 t.digAndUp()
 t.digAndUp()
 t.digAndUp()
end

if usechest then
 emptyslots()
end

msg = "sandie: Finished "..mylength.."x"..mywidth.."x"..mydepth.."="..mylength*mywidth*mydepth
if usechest then
 msg = msg.." in enderchest"
end
p.send(msg)