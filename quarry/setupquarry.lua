-- http://pastebin.com/YkH0UmMD
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX

-- Build 4 quarry platforms, 64x64

print ("Slot 1: 16 dirt")
print ("Slot 2: 3 land mark's")
print ("Slot 3: 1 quarry")
print ("Slot 4: 1 item tesseract (output)")
print ("Slot 5: 1 energy tesseract (input)")
print ("Press key to start")
local dum = read()

local function buildPlatform(landmark)

    t.down()

    t.select(1)
    local i = 4
    while i > 0 do
        t.placeDown()
        t.digAndForward()
        if i > 1 then
            t.turnLeft() -- last time don't turn
        end
        i = i - 1
    end

    t.up()

    landmark = landmark or false
    if landmark then
        t.select(2)
        t.placeDown() -- landmark
    end

end

function moveToNextPlatform()
    for i = 1, 63 do -- 1 less than 64 !
        t.digAndForward()
    end
end

-- READ PARAMETERS
local args = { ... }
if #args == 1 then
    for i=1, args[1] do
        t.digAndUp()
    end
end

t.digAndUp()
t.turnLeft()

buildPlatform(true)
moveToNextPlatform()

buildPlatform(true)
moveToNextPlatform()
buildPlatform(false)
moveToNextPlatform()
buildPlatform(true)
moveToNextPlatform()

t.digAndForward()

t.select(3) -- Quarry
t.placeDown()

t.digAndUp()

t.select(4) -- Item Tesseract
t.placeDown()

t.turnLeft()
t.digAndForward()
t.digAndDown()
t.select(5) -- Energy Tesseract
t.placeDown()
