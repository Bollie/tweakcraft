E = peripheral.wrap("Extractor_2")
G = peripheral.wrap("AdvancedGears_1")

function speed(slot, a)
	G.setRatio(32)
	print("Slot "..slot.."="..a.." => Speed")
end

function torque(slot, a)
	G.setRatio(1)
	print("Slot "..slot.."="..a.." => Torque")
end

while true do
    p, t, s = G.getPower()
	
	A, B, C = E.getSlot(3)
	if (C or 0) > 0 then
		torque(3, C)
	else
		A, B, C = E.getSlot(2)
		if (C or 0) > 0 then
			speed(2, C)
		else
			A, B, C = E.getSlot(1)
			if (C or 0) > 0 then
				speed(1, C)
			else
				A, B, C = E.getSlot(0)
				if (C or 0) > 0 then
					torque(0, C)
				end
			end
		end
	end
	
	os.sleep(15)
end