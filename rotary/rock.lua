-- http://pastebin.com/7ag091Gh
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX

-- READ PARAMETERS
local args = { ... }
 
-- CHECK SLOTS AND PARAMETERS
local argsOK = true
 
if #args < 1 then
 argsOK = false
else
 -- read length
 mylength = math.floor(tonumber(args[1]) or 1)
end
 
if argsOK == false then
 print("usage: rock <length>")
 print("put machines in slots")
 return
end

-- put turtle facing wall
SLOT_BEDROCK_BREAKER = 16
SLOT_BEDROCK_2_1_GEAR = 15
SLOT_INDUSTRIAL_COIL = 14
SLOT_PERIPHERAL_PROXY = 13
BLOCKS_TO_GRIND = 1 -- normal 4
turn = 1

while turn <= mylength do

 t.turn()
 t.forward()
 t.forward()
 
 t.select(SLOT_INDUSTRIAL_COIL)
 t.place()
 t.back()
 t.select(SLOT_PERIPHERAL_PROXY)
 t.place()
 
 coil = peripheral.wrap("front")
 coil.setTorque(4096)
 coil.setSpeed(512)
 
 t.select(SLOT_PERIPHERAL_PROXY)
 t.dig()
 
 t.select(SLOT_BEDROCK_2_1_GEAR)
 t.place()
 
 t.back()
 t.select(SLOT_BEDROCK_BREAKER)
 t.place()
 
 t.up()
 t.digAndForward() -- bedrock breaker
 t.digAndForward() -- bedrock 2:1 gear
 t.digAndForward() -- industrial coil
 
 redstone.setOutput("bottom", true)
 
-- bock can be till 4, but to lower risk only 1 here
 for block = 1, BLOCKS_TO_GRIND do
  for slice = 1, 17 do
   print("block: "..block.." slice: "..slice)
   sleep(18)
  end
 end
 
 redstone.setOutput("bottom", false)
 
 t.select(SLOT_INDUSTRIAL_COIL)
 t.digDown()
 t.down()
 t.turn()
 t.select(SLOT_BEDROCK_2_1_GEAR)
 t.dig()
 t.forward()
 t.select(1)
 t.suck()
 
 t.select(SLOT_BEDROCK_BREAKER)
 t.dig()
 t.select(1)
 t.suck()

 for i=1, 4 do
  t.turnRight()
  t.suck()
 end
 t.forward()

 for i=1, 4 do
  t.turnRight()
  t.suck()
 end
 t.forward()

 for i=1, 4 do
  t.turnRight()
  t.suck()
 end

 for i=1, BLOCKS_TO_GRIND do
  t.forward()
 end

 turn = turn + 1
end