os.loadAPI("p")
os.loadAPI("t")
os.loadAPI("json")

-- config
url= "http://home.bolukan.nl/minecraft/minecraft.php"

-- function
local function writetest(prettyjson)
	local h = fs.open("test/test", "w")
	h.write(prettyjson, "a")
	h.close()
end

local function postjson(prettyjson, pe)
	http.post(url, "msg="..prettyjson.."&name="..pe.getInventoryName())
end

local function logitemslots()
v = 1
while v <= 16 do
	if t.getItemCount(v) ~= 0 then
		id = t.getItemDetail(v)
		if id ~= nil then
			p.send(json.encode(id))
		end
	end
	v=v+1
end

local pe = peripheral.wrap("front")
local lss = pe.getAdvancedMethodsData()
local pjt = json.encodePretty(lss)

-- log to monitor
p.send("Name: "..pe.getInventoryName().." Size: "..pe.getInventorySize())
p.send(pjt)
-- write to file and post to internet
writetest(pjt)
postjson(pjt, pe)

