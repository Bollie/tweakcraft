-- http://pastebin.com/HdkYq3Pw
-- http://pastebin.com/u/Bollie
os.loadAPI("p") -- http://pastebin.com/SGcYmCqd
os.loadAPI("t") -- http://pastebin.com/6vxbgdjX

local mylength = 1
local mywidth = 1
local mydepth = 1
local usechest = false
local ENDERCHESTSLOT = 16

-- READ PARAMETERS
local args = { ... }
local argsOK = true

-- read length, width and depth (this is 1/3 of real depth. So put in 2 for digging 6, etc)
mylength = math.floor(tonumber(args[1]) or 1)
mywidth = math.floor(tonumber(args[2]) or 1)
mydepth = math.floor(tonumber(args[3]) or 64)

-- do sanity check, upper boundary is arbitrairy, but protects user for bad input
if (mylength < 1) or (mywidth < 1) or (mydepth < 1) then argsOK = false end
if (mylength > 64) or (mywidth > 64) or (mydepth > 64) then argsOK = false end 
-- check for ender chest
if (t.getItemCount(ENDERCHESTSLOT) == 1) then 
 usechest = true
 print("Enderchest: YES (slot "..ENDERCHESTSLOT.." = 1")
else
 print("NO enderchest. (slot "..ENDERCHESTSLOT.." <> 1")
end

if argsOK == false then
 print("put ender chest in slot "..ENDERCHESTSLOT..".")
 print("usage: bed <length> <width> <optional depth (*3)>")
 return
end
-- END PARAMETERS

function toBedrock()
 local bedrock = false
 local tries = 0
 
 while (bedrock ~= true) and (tries < 20) do
 
  -- remove block
  if t.detectDown() then -- block detected
   if (t.digDown() == false) then
    return t.posy()
   end
  end

  -- descent, if not successfull attack max 20 times before giving up.
  tries = 0
  while (t.down() == false) and (tries < 20) do
   t.attackDown()
   tries = tries + 1
   os.sleep(1)
  end 

 end
 return t.posy()
 
end

-- FUNCTION HARVEST
function harvest(width, length, currentdepth)
 for w = 1, width do
  for l = 1, length do
   -- dig
   t.select(1)
   if t.compareDown() == false then
    t.digDown()
   end
   t.select(1)
   if t.compareUp() == false then
    t.digsUp()
   end
   t.suckDown()
   emptyslots(15) -- only empty all slots when slot 10 is not empty anymore
   -- goto next location
   if l < length then
    t.digAndForward()
   else
    -- end of length, turn (except for last w)
    if w < width then
     if ((currentdepth + 1) * (width + 1) + w) % 2 == 0 then
      t.turnLeft()
      t.digAndForward()
      t.turnLeft()
     else
      t.turnRight()
      t.digAndForward()
      t.turnRight()
     end -- turn
    end -- last w
   end --last l
  end -- all l(ength)
 end -- all w(idth)
end

-- FUNCTION RETURNHOME
function returnhome(width, length, currentdepth)
 -- if uneven, return to begin l)
 if (currentdepth % 2 == 1) and (width % 2 == 1) then
  t.turn()
  for l = 1, length - 1 do
   t.digAndForward()
  end
 end
 -- return to home
 if (currentdepth % 2 == 1) then
  t.turnRight()
  for w = 1, width - 1 do
   t.digAndForward()
  end
  t.turnRight()
 end
end

-- FUNCTION EMPTYSLOTS
function emptyslots(checkslot)
 if checkslot ~= nil then
  if t.getItemCount(checkslot) == 0 then
   return
  end   
 end
 if usechest == true then
  t.digsUp()
  t.select(ENDERCHESTSLOT)
  t.placeUp()
  t.dropAll(t.dropUp, 1, 1, 1)
  t.dropAll(t.dropUp, 0, 2, 16)
  t.select(ENDERCHESTSLOT)
  t.digUp()
  t.select(1)
 else
  tmp_x = t.posx()
  tmp_y = t.posy()
  tmp_z = t.posz()
  tmp_f = t.posf()
 
  t.gotoPos(0, 0, 0, 2)
  t.dropAll(t.drop, 1, 1, 1)
  t.dropAll(t.drop, 0, 2, 16)
  t.select(1)
  t.gotoPos(tmp_x, tmp_y, tmp_z, tmp_f)

 end
end 

-- MAIN

msg = "bed: Start "..mylength.."x"..mywidth.."="..mylength*mywidth
if usechest then
 msg = msg.." in enderchest"
end
p.send(msg)

toBedrock()
t.digAndUp()

d = 1
while t.posy() < 0 do
 t.select(1)
 harvest(mywidth, mylength, d)
 -- next layer
 if (t.posy() < -3) and (d < mydepth) then
  t.digAndUp()
  t.digAndUp()
  t.digAndUp()
  t.turn()
  d = d + 1
 else
  -- get to shaft
  returnhome(mywidth, mylength, d)
  -- get up
  while t.posy() < 0 do
   t.digAndUp()
  end
 end
end

-- empty all
if usechest then
 emptyslots()
end

msg = "bed: Finished "..mylength.."x"..mywidth.."x"..d.."="..mylength*mywidth*d
if usechest then
 msg = msg.." in enderchest"
end
p.send(msg)