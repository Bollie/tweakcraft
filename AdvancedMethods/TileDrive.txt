test<msg>{
	"getInventorySize": {
		"returnTypes": [
			"NUMBER"
		],
		"args": [
		],
		"description": "Get the size of this inventory",
		"source": "inventory"
	},
	"getStackInSlot": {
		"returnTypes": [
			"TABLE"
		],
		"args": [
			{
				"type": "NUMBER",
				"name": "slotNumber",
				"description": "The slot number, from 1 to the max amount of slots"
			}
		],
		"description": "Get details of an item in a particular slot",
		"source": "inventory"
	},
	"getAllStacks": {
		"returnTypes": [
			"TABLE"
		],
		"args": [
		],
		"description": "Get a table with all the items of the chest",
		"source": "inventory"
	},
	"listSources": {
		"returnTypes": [
			"TABLE"
		],
		"args": [
		],
		"description": "List all method sources",
		"source": "<meta>"
	},
	"pullItemIntoSlot": {
		"returnTypes": [
			"NUMBER"
		],
		"args": [
			{
				"type": "STRING",
				"name": "direction",
				"description": "The direction of the other inventory. (north, south, east, west, up or down)"
			},
			{
				"type": "NUMBER",
				"name": "slot",
				"description": "The slot in the OTHER inventory that you're pulling from"
			},
			{
				"optional": true,
				"description": "The maximum amount of items you want to pull",
				"type": "NUMBER",
				"name": "maxAmount"
			},
			{
				"optional": true,
				"description": "The slot in the current inventory that you want to pull into",
				"type": "NUMBER",
				"name": "intoSlot"
			}
		],
		"description": "Pull an item from a slot in another inventory into a slot in this one. Returns the amount of items moved",
		"source": "inventory-world"
	},
	"getAdvancedMethodsData": {
		"returnTypes": [
			"TABLE"
		],
		"args": [
		],
		"description": "Get a complete table of information about all available methods",
		"source": "<meta>"
	},
	"swapStacks": {
		"returnTypes": [
		],
		"args": [
			{
				"type": "NUMBER",
				"name": "from",
				"description": "The first slot"
			},
			{
				"type": "NUMBER",
				"name": "to",
				"description": "The other slot"
			},
			{
				"optional": true,
				"description": "",
				"type": "STRING",
				"name": "fromDirection"
			},
			{
				"optional": true,
				"description": "",
				"type": "STRING",
				"name": "fromDirection"
			}
		],
		"description": "Swap two slots in the inventory",
		"source": "inventory"
	},
	"condenseItems": {
		"returnTypes": [
		],
		"args": [
		],
		"description": "Condense and tidy the stacks in an inventory",
		"source": "inventory"
	},
	"expandStack": {
		"returnTypes": [
			"TABLE"
		],
		"args": [
			{
				"type": "TABLE",
				"name": "stack",
				"description": ""
			}
		],
		"description": "",
		"source": "inventory"
	},
	"getInventoryName": {
		"returnTypes": [
			"STRING"
		],
		"args": [
		],
		"description": "Get the name of this inventory",
		"source": "inventory"
	},
	"listMethods": {
		"returnTypes": [
			"STRING"
		],
		"args": [
		],
		"description": "List all the methods available",
		"source": "<meta>"
	},
	"destroyStack": {
		"returnTypes": [
		],
		"args": [
			{
				"type": "NUMBER",
				"name": "slotNumber",
				"description": "The slot number, from 1 to the max amount of slots"
			}
		],
		"description": "Destroy a stack",
		"source": "inventory"
	},
	"pullItem": {
		"returnTypes": [
			"NUMBER"
		],
		"args": [
			{
				"type": "STRING",
				"name": "direction",
				"description": "The direction of the other inventory. (north, south, east, west, up or down)"
			},
			{
				"type": "NUMBER",
				"name": "slot",
				"description": "The slot in the OTHER inventory that you're pulling from"
			},
			{
				"optional": true,
				"description": "The maximum amount of items you want to pull",
				"type": "NUMBER",
				"name": "maxAmount"
			},
			{
				"optional": true,
				"description": "The slot in the current inventory that you want to pull into",
				"type": "NUMBER",
				"name": "intoSlot"
			}
		],
		"description": "Pull an item from a slot in another inventory into a slot in this one. Returns the amount of items moved",
		"source": "inventory-world"
	},
	"pushItem": {
		"returnTypes": [
			"NUMBER"
		],
		"args": [
			{
				"type": "STRING",
				"name": "direction",
				"description": "The direction of the other inventory. (north, south, east, west, up or down)"
			},
			{
				"type": "NUMBER",
				"name": "slot",
				"description": "The slot in the current inventory that you're pushing from"
			},
			{
				"optional": true,
				"description": "The maximum amount of items you want to push",
				"type": "NUMBER",
				"name": "maxAmount"
			},
			{
				"optional": true,
				"description": "The slot in the other inventory that you want to push into",
				"type": "NUMBER",
				"name": "intoSlot"
			}
		],
		"description": "Push an item from the current inventory into slot on the other one. Returns the amount of items moved",
		"source": "inventory-world"
	},
	"pushItemIntoSlot": {
		"returnTypes": [
			"NUMBER"
		],
		"args": [
			{
				"type": "STRING",
				"name": "direction",
				"description": "The direction of the other inventory. (north, south, east, west, up or down)"
			},
			{
				"type": "NUMBER",
				"name": "slot",
				"description": "The slot in the current inventory that you're pushing from"
			},
			{
				"optional": true,
				"description": "The maximum amount of items you want to push",
				"type": "NUMBER",
				"name": "maxAmount"
			},
			{
				"optional": true,
				"description": "The slot in the other inventory that you want to push into",
				"type": "NUMBER",
				"name": "intoSlot"
			}
		],
		"description": "Push an item from the current inventory into slot on the other one. Returns the amount of items moved",
		"source": "inventory-world"
	}
}</msg>
<name>TileDrive</name>
